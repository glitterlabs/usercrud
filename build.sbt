name := """userCRUD"""

version := "0.0.1-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc, javaJpa.exclude("org.hibernate.javax.persistence", "hibernate-jpa-2.0-api"),
  "org.hibernate" % "hibernate-entitymanager" % "4.3.10.Final",
  "javax.xml" % "jaxb-api" % "2.1",
  "org.json"%"org.json"%"chargebee-1.0",
  "mysql" % "mysql-connector-java" % "5.1.34"
)

lazy val root = (project in file(".")).enablePlugins(PlayJava)

resolvers ++= Seq(
"Apache" at "http://repo1.maven.org/maven2/",
"Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
 "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"
)