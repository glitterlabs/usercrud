package controllers;

import java.util.Date;

import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import models.entities.GlitterlabsAccount;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import com.fasterxml.jackson.databind.JsonNode;

import controllers.exception.InvalidUserException;

public class AccountController extends Controller {

	private static final String MESSAGE_PASSWORD_REQUIRED = "password required";
	private static final String MESSAGE_USER_CREATED_WITH_ID = "user created with id";
	private static final String MESSAGE_EMAIL_OR_PHONE_NUMBER_IS_REQUIRED = "Either email or phone number is required";
	private static final String MESSAGE_USER_REMOVED = "user removed";

	/**
	 * Gets the account.
	 *
	 * @param id the account id
	 * @return the user account
	 */
	@Transactional(readOnly = true)
	public static Result getAccount(final Integer id) {
		GlitterlabsAccount account = getAccountByPrimaryKey(id);
		if (account != null) {
			JsonNode node = Json.toJson(account);
			return ok(node);
		}
		return ok("user with id " + id + " not found");
	}

	/**
	 * Gets the account by primary key.
	 *
	 * @param id the id
	 * @return the account by primary key
	 */
	private static GlitterlabsAccount getAccountByPrimaryKey(final Integer id) {
		return JPA.em().find(GlitterlabsAccount.class, id);
	}

	/**
	 * Creates the account.
	 *
	 * @return the result
	 */
	@Transactional
	public static Result createAccount() {
		Form<GlitterlabsAccount> form = Form.form(GlitterlabsAccount.class).bindFromRequest();
		GlitterlabsAccount account = form.get();

		setTodayInEntity(account);
		try {
			validateOnCreate(account);
			JPA.em().persist(account);
		} catch (PersistenceException exception) {
			Throwable th = getExactException(exception);
			return badRequest(th.getMessage());
		} catch (InvalidUserException e) {
			return badRequest(e.getMessage());
		}
		return ok(MESSAGE_USER_CREATED_WITH_ID + " " + account.getAccountId());
	}

	/**
	 * Validate account data on create.
	 *
	 * @param account the account
	 * @throws InvalidUserException the invalid user exception
	 */
	private static void validateOnCreate(GlitterlabsAccount account) throws InvalidUserException {
		if (account.getAccountPhone() == null && account.getAccountEmail() == null) {
			throw new InvalidUserException(MESSAGE_EMAIL_OR_PHONE_NUMBER_IS_REQUIRED);
		}
		if (account.getAccountEmail() != null && account.getAccountPassword() == null) {
			throw new InvalidUserException(MESSAGE_PASSWORD_REQUIRED);
		}
	}

	private static Throwable getExactException(Exception exception) {
		Throwable th = exception;
		while (th.getCause() != null && th.getCause() != th.getCause().getCause()) {
			th = th.getCause();
		}
		return th;
	}

	/**
	 * Sets today date in entity.
	 *
	 * @param glitterlabsAccount the new entity
	 */
	private static void setTodayInEntity(GlitterlabsAccount glitterlabsAccount) {
		Date today = new Date();
		glitterlabsAccount.setCreationDate(today);
		glitterlabsAccount.setModificationDate(today);
	}

	/**
	 * Gets all the account.
	 *
	 * @return all the user account
	 */
	@Transactional(readOnly = true)
	public static Result getAllAccounts() {
		CriteriaBuilder cb = JPA.em().getCriteriaBuilder();
		CriteriaQuery<GlitterlabsAccount> cq = cb.createQuery(GlitterlabsAccount.class);
		Root<GlitterlabsAccount> root = cq.from(GlitterlabsAccount.class);
		CriteriaQuery<GlitterlabsAccount> all = cq.select(root);
		TypedQuery<GlitterlabsAccount> allQuery = JPA.em().createQuery(all);
		JsonNode jsonNodes = Json.toJson(allQuery.getResultList());
		return ok(jsonNodes);
	}

	/**
	 * Delete account.
	 *
	 * @param id the account id
	 * @return the result
	 */
	@Transactional
	public static Result delete(final Integer id) {
		JPA.em().remove(getAccountByPrimaryKey(id));
		return ok(MESSAGE_USER_REMOVED);
	}

	/**
	 * Updates the account.
	 *
	 * @param id the account id
	 * @return the user account
	 */
	@Transactional()
	public static Result updateAccount(final Integer id) {
		GlitterlabsAccount accountFromDB = getAccountByPrimaryKey(id);
		Form<GlitterlabsAccount> form = Form.form(GlitterlabsAccount.class).bindFromRequest();
		GlitterlabsAccount formAccount = form.get();
		copyUpdateData(accountFromDB, formAccount);
		try {
			JPA.em().persist(accountFromDB);
		} catch (PersistenceException exception) {
			return badRequest(getExactException(exception).getMessage());
		}
		JsonNode node = Json.toJson(accountFromDB);
		return ok(node);
	}

	/**
	 * Copy update data.
	 *
	 * @param accountFromDB the account from db
	 * @param formAccount the form account
	 */
	private static void copyUpdateData(GlitterlabsAccount accountFromDB, GlitterlabsAccount formAccount) {
		if (formAccount.getAccountEmail() != null) {
			accountFromDB.setAccountEmail(formAccount.getAccountEmail());
		}
		if (formAccount.getAccountName() != null) {
			accountFromDB.setAccountName(formAccount.getAccountName());
		}
		if (formAccount.getAccountPhone() != null) {
			accountFromDB.setAccountPhone(formAccount.getAccountPhone());
		}
		accountFromDB.setModificationDate(new Date());
	}
}
