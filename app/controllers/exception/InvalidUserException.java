package controllers.exception;

/**
 * The Exception Class which is thrown when user provides invalid details.
 */
public class InvalidUserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidUserException(final String message) {
		super(message);
	}

	/**
	 * Instantiates a new invalid user exception.
	 */
	public InvalidUserException() {
		super();
	}
}
