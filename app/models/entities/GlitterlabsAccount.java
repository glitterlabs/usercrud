package models.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the glitterlabs_account database table.
 * 
 */
@XmlRootElement(name = "GlitterlabsAccount")
@Entity
@Table(name = "glitterlabs_account")
public class GlitterlabsAccount implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The account id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ACCOUNT_ID")
	private int accountId;

	/** The creation date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	/** The modification date. */
	@Temporal(TemporalType.DATE)
	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;

	/** The account email. */
	@Column(name = "ACCOUNT_EMAIL")
	private String accountEmail;

	/** The account password. */
	@XmlTransient
	@JsonIgnore
	@Column(name = "ACCOUNT_PASSWORD")
	private String accountPassword;

	/** The account name. */
	@Column(name = "ACCOUNT_NAME")
	private String accountName;

	/** The account phone. */
	@Column(name = "ACCOUNT_Phone")
	private Long accountPhone;

	/**
	 * Instantiates a new glitterlabs account.
	 */
	public GlitterlabsAccount() {
	}

	/**
	 * Gets the account id.
	 *
	 * @return the account id
	 */
	public int getAccountId() {
		return this.accountId;
	}

	/**
	 * Sets the account id.
	 *
	 * @param accountId the new account id
	 */
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date.
	 *
	 * @return the modification date
	 */
	public Date getModificationDate() {
		return this.modificationDate;
	}

	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate the new modification date
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * Gets the account email.
	 *
	 * @return the account email
	 */
	public String getAccountEmail() {
		return this.accountEmail;
	}

	/**
	 * Sets the account email.
	 *
	 * @param accountEmail the new account email
	 */
	public void setAccountEmail(String accountEmail) {
		this.accountEmail = accountEmail;
	}

	/**
	 * Gets the account password.
	 *
	 * @return the account password
	 */
	@XmlTransient
	public String getAccountPassword() {
		return this.accountPassword;
	}

	/**
	 * Sets the account password.
	 *
	 * @param accountPassword the new account password
	 */
	public void setAccountPassword(String accountPassword) {
		this.accountPassword = accountPassword;
	}

	/**
	 * Gets the account name.
	 *
	 * @return the account name
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * Sets the account name.
	 *
	 * @param accountName the new account name
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * Gets the account phone.
	 *
	 * @return the account phone
	 */
	public Long getAccountPhone() {
		return accountPhone;
	}

	/**
	 * Sets the account phone.
	 *
	 * @param accountPhone the new account phone
	 */
	public void setAccountPhone(Long accountPhone) {
		this.accountPhone = accountPhone;
	}

}